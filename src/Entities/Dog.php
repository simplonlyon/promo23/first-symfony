<?php

namespace App\Entities;
use DateTime;
use Symfony\Component\Validator\Constraints as Assert;


class Dog {
    private ?int $id;
	#[Assert\NotBlank]
	private ?string $name;

	#[Assert\NotBlank(allowNull: true)]
    private ?string $breed;
	#[Assert\Date]
    private ?DateTime $birthdate;


	/**
	 * @param int|null $id
	 * @param string|null $name
	 * @param string|null $breed
	 * @param DateTime|null $birthdate
	 */
	public function __construct(?string $name = null, ?string $breed = null, ?DateTime $birthdate = null, ?int $id = null) {
		$this->id = $id;
		$this->name = $name;
		$this->breed = $breed;
		$this->birthdate = $birthdate;
	}


	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getName(): ?string {
		return $this->name;
	}
	
	/**
	 * @param string|null $name 
	 * @return self
	 */
	public function setName(?string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getBreed(): ?string {
		return $this->breed;
	}
	
	/**
	 * @param string|null $breed 
	 * @return self
	 */
	public function setBreed(?string $breed): self {
		$this->breed = $breed;
		return $this;
	}
	
	/**
	 * @return DateTime|null
	 */
	public function getBirthdate(): ?DateTime {
		return $this->birthdate;
	}
	
	/**
	 * @param DateTime|null $birthdate 
	 * @return self
	 */
	public function setBirthdate(?DateTime $birthdate): self {
		$this->birthdate = $birthdate;
		return $this;
	}
}