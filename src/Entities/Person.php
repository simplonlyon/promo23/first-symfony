<?php

namespace App\Entities;
use Symfony\Component\Validator\Constraints as Assert;

class Person {
    private ?int $id;
	#[Assert\NotBlank]
	private ?string $firstname;
	#[Assert\NotBlank]
    private ?string $name;

	/**
	 * @param int|null $id
	 * @param string|null $firstname
	 * @param string|null $name
	 */
	public function __construct(?string $firstname = null, ?string $name = null, ?int $id = null) {
		$this->id = $id;
		$this->firstname = $firstname;
		$this->name = $name;
	}


	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getFirstname(): ?string {
		return $this->firstname;
	}
	
	/**
	 * @param string|null $firstname 
	 * @return self
	 */
	public function setFirstname(?string $firstname): self {
		$this->firstname = $firstname;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getName(): ?string {
		return $this->name;
	}
	
	/**
	 * @param string|null $name 
	 * @return self
	 */
	public function setName(?string $name): self {
		$this->name = $name;
		return $this;
	}

}