<?php

namespace App\Controller;

use App\Entities\Dog;
use App\Repository\DogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * On peut définir une route globale sur le contrôleur directement, ici toutes les routes
 * de ce contrôleur seront préfixées par /api/dog
 */
#[Route('/api/dog')]
class DogController extends AbstractController
{
    private DogRepository $repo;
    /**
     * On utilise ici l'injection de dépendance de Symfony qui va, en se basant sur le type de
     * l'argument, se charger de créer une instance de la classe voulue
     * On peut aussi faire de l'injection dans les méthodes directement, chose qu'on fait en dessous
     * par exemple avec la Request et le SerializerInterface (ces deux classes spécifiques, on ne sait
     * même pas trop comment en faire des instances nous même, mais Symfony lui le sait, et s'en charge)
     */
    public function __construct(DogRepository $repo)
    {
        $this->repo = $repo;
    }
    /**
     * On indique ici que cette route n'est accessible que via une méthode GET, pas la peine de
     * répéter l'url grâce à la route globale du contrôleur
     */
    #[Route(methods: 'GET')]
    public function all()
    {

        $dogs = $this->repo->findAll();
        return $this->json($dogs);
    }
    /**
     * Cette route va utiliser le paramètre id (sur la route /api/dog/1 par exemple, en GET) pour
     * récupérer le chien correspondant dans le repository, si celui ci n'existe pas, on throw une NotFoundException
     * qui permettra à symfony de généré une erreur 404
     */
    #[Route('/{id}', methods: 'GET')]
    public function one(int $id)
    {
        $dog = $this->repo->findById($id);
        if (!$dog) {
            throw new NotFoundHttpException();

        }
        return $this->json($dog);
    }

    /**
     * Dans cette route, on récupère le body (le contenu) de la requête POST au format JSON et on
     * utilise le composant Serializer de Symfony pour transformer ce JSON en une instance de la classe
     * voulue, en l'occurrence un Dog. On le fait ensuite persister et on renvoie le chien complet avec
     * son id générée en indiquant un code 201 qui signifie "succés, ressource créé"
     */
    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        try {
            $dog = $serializer->deserialize($request->getContent(), Dog::class, 'json');
            $this->repo->persist($dog);

            return $this->json($dog, Response::HTTP_CREATED);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }


    }

    /**
     * Ne faites pas ça chez vous
     * (ça va ça marche, juste le PUT en dessous marche aussi et est plus simple à comprendre et au final
     * vu comment va marcher notre javascript, ce patch n'est pas vraiment nécessaire)
     */
    #[Route('/{id}', methods: 'PATCH')]
    public function update(int $id, Request $request, SerializerInterface $serializer)
    {
        $dog = $this->repo->findById($id);
        if (!$dog) {
            throw new NotFoundHttpException();
        }
        $dogOld = json_decode($serializer->serialize($dog, 'json'), true);

        $newValue = json_encode([...$dogOld, ...$request->toArray()]);
        try {

            $toUpdate = $serializer->deserialize($newValue, Dog::class, 'json');

            $this->repo->update($toUpdate);

            return $this->json($toUpdate);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);

        }
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $dog = $this->repo->findById($id);
        if (!$dog) {
            throw new NotFoundHttpException();
        }
        try {


            $toUpdate = $serializer->deserialize($request->getContent(), Dog::class, 'json');
            $toUpdate->setId($id);
            $this->repo->update($toUpdate);

            return $this->json($toUpdate);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id)
    {
        $dog = $this->repo->findById($id);
        if (!$dog) {
            throw new NotFoundHttpException();
        }

        $this->repo->delete($dog);

        return $this->json(null, Response::HTTP_NO_CONTENT);

    }


}