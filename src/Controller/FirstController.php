<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Un contrôleur en symfony va servir à plusieurs choses :
 * Il fait le lien entre la couche "présentation" et la couche database pour faire en sorte que les
 * données qui arrivent aux repositories soient validées et correctes, que certaines données ne soient
 * accessible qu'à certains profils sous certaines conditions, etc.
 * Dans Symfony (et la plupart des framework MVC), les contrôleurs serviront également à définir les
 * routes : les url et les méthodes http qui permettront d'accéder à une fonction ou une autre
 * Si on définit une route "/", la fonction en dessous sera déclenchée quand on ira sur http://localhost:8000
 * (dans le cas d'un projet qui tourne en local, bien sûr)
 */
class FirstController extends AbstractController {

    #[Route('/')]
    public function homepage() {
        
        return $this->json('coucou');
    }
    /**
     * On peut définir une route avec un paramètre qui sera récupéré par une variable dans la fonction
     * avec le même nom. Ici, si je vais sur http://localhost:8000/greet/test, je tomberai sur cette fonction
     * et la valeur de $name sera 'test
     */
    #[Route('greet/{name}')]
    public function greet(string $name) {
        return $this->json('hello '.$name);
    }
}