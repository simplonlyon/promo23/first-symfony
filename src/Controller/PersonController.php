<?php

namespace App\Controller;

use App\Entities\Person;
use App\Form\PersonType;
use App\Repository\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/person')]
class PersonController extends AbstractController
{
    public function __construct(private PersonRepository $repo) {}

    #[Route( methods:'GET')]
    public function all(Request $request): JsonResponse
    {
        $page = $request->query->get('page', 1);
        $pageSize = $request->query->get('pageSize', 15);
        return $this->json(
            $this->repo->findAll($page, $pageSize)
        );
    }
    #[Route(methods: 'POST')]
    public function add(Request $request ): JsonResponse {

        /**
         * une autre manière de gérer la transformation du body en instance d'entité tout en faisant
         * les validations est d'utiliser les composant Form/Type de symfony, on crée une classe Type
         * dans laquelle on indique les champs qu'on va récupérer dans la requête
         */        
        $form = $this->createForm(PersonType::class);
        //On donne le contenu du body au formulaire
        $form->submit($request->toArray());
        //On vérifie si celui ci est valide
        if(!$form->isValid()) {
            //Sinon on renvoie une erreur 400 avec les erreurs
            return $this->json($form, Response::HTTP_BAD_REQUEST);
        }
        //On récupère l'instance de l'entité crée si tout est ok
        $person = $form->getData();
        //On la fait persister
        $this->repo->persist($person);

        return $this->json($person, Response::HTTP_CREATED);

    }

    
}
