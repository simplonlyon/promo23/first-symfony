# First Symfony

Premier projet symfony, une API REST avec du PDO


## How To Use
1. Faire un `composer install`
2. Importer la base de données où vous le souhaitez (par exemple : `sudo mysql p23_first < database.sql`)
3. Créer un fichier `.env.local` avec ça dedans, en remplaçant si besoin les informations de connexion à la base de données par les votre :
```
DATABASE_HOST=localhost
DATABASE_NAME=p23_first
DATABASE_USERNAME=simplon
DATABASE_PASSWORD=1234
```
4. Lancer le serveur avec `symfony server:start`

## Validation
Pour mettre en place la validation des entités dans un nouveau projet, installer : `composer req validator serializer`

Ensuite copier le dossier Serializer dans votre src (pour mettre la classe ValidatorDenormalizer dedans).

On peut ensuite ajouter des validateur sur ses propriétés d'entité :
```php
use Symfony\Component\Validator\Constraints as Assert;

class Dog {
    private ?int $id;
	#[Assert\NotBlank]
	private ?string $name;
	#[Assert\NotBlank(allowNull: true)]
    private ?string $breed;
	#[Assert\Date]
    private ?DateTime $birthdate;
}
```

Et dans le contrôleur, on try-catch la deserialization pour renvoyer une erreur si besoin : 
```php
//Dans une route
try {
    $dog = $serializer->deserialize($request->getContent(), Dog::class, 'json');
    
    //...faire ce qu'on doit  faire avec le chien

    return $this->json($dog);

} catch (ValidationFailedException $e) {
    return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
} catch (NotEncodableValueException $e) {
    return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
}
```

## Exercices

### Mini exo route :
1. Dans le FirstController, rajouter une nouvelle route sur /greet qui va renvoyer en json "hello" suivi d'un prénom
2. Modifier la route, pour en faire une route paramétrée qui permettra d'aller par exemple sur http://localhost:8000/greet/jean et qui affichera donc "hello jean" (donc en gros trouver comment rajouter un paramètre de route et le récupérer de l'url à l'intérieur de la méthode) 

### Contrôleur de chiens
1. Si c'est pas déjà fait, récupérer les entities Person et Dog, et les repository associés dans le projet précédent et les mettre dans le src
2. Créer un nouveau contrôleur DogController et dedans, faire 2 route, une sur /api/dog et l'autre sur /api/dog/id 
3. Faire que la première utilise le repository dog pour renvoyer tous les chiens
4. Faire que la seconde utilise le repository dog pour renvoyer un chien spécifique par son id
   
**Bonus :** Faire que sur le truc par id, si jamais ya pas de chien correspondant, on renvoie un 404

### Contrôleur des personnes
1. En s'inspirant du DogController, utiliser l'entité et le repository person fait dans le projet php-pdo pour faire un contrôleur pour les Person
2. Faire en sorte de pouvoir gérer la pagination via le contrôleur
